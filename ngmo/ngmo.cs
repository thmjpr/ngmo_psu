﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace ngmo_psu
{
    class ngmo
    {
        public SerialPort port = null;
        public bool connected = false;
        private string op;
        public string channel = channels.CH_A;

        public struct channels
        {
            public static string CH_A = ":A";
            public static string CH_B = ":B";
        }

        public enum bandwidth
        {
            LOW, HIGH
        }

        //Set either trip on overcurrent, or limit
        public enum current_trip
        {
            LIMIT, TRIP,
        }

        //Set reading range for current: 5A, 0.5A, 5mA
        public enum current
        {
            AUTO, HIGH, MED, LOW
        }

        public enum sample_ch
        { CURR, DVM }

        public enum trig_slope
        { POS, NEG }

        public enum trig_source
        { INT, EXT }

        //long integer, short real, double real
        public enum data_format
        { ASCII, LONG, SREAL, DREAL }

        public ngmo()
        {

        }

        //Bitfield of Condition Register (:COND?)
        private enum meas_COND
        {
            None = 0,
            Reading_ovf_ch_a = 1 << 3,
            Pulse_trig_timeout_ch_a = 1 << 4,
            Reading_available_ch_a = 1 << 5,
            Reading_ovf_ch_b = 1 << 6,
            Pulse_trig_timeout_ch_b = 1 << 7,
            Reading_available_ch_b = 1 << 8,
            Measure_aborted_ch_a = 1 << 9,
            Measure_aborted_ch_b = 1 << 10,
        }

        //init could do a read and populate everything how the psu is setup
        //---------------------------------------------------------
        public string initialize(SerialPort comm)
        {
            port = comm;
            port.ReadTimeout = 2000;
            port.WriteTimeout = 500;
            port.DtrEnable = true;

            string id = get_id();

            if (id.StartsWith("ROHDE&SCHWARZ,NGMO"))
            {
                connected = true;
                reset();
                set_current_res(current.AUTO);
            }

            else
                connected = false;

            return id;
        }


        //---------------------------------------------------------
        string get_id()
        {
            string id = "";
            write("*IDN?");
            id = read();
            return id;
        }

        //---------------------------------------------------------
        void reset()
        {
            write("*RST");
        }

        //---------------------------------------------------------
        // Clear all event registers and status queues
        public void clear_all()
        {
            write("*CLS");
        }

        //---------------------------------------------------------
        public bool get_power()
        {
            return false;
        }

        //---------------------------------------------------------
        //
        public double get_voltage()
        {
            write(":SENS:FUNC VOLT");
            return read_single();
        }

        //---------------------------------------------------------
        //
        public double get_current()
        {
            write(":SENS:FUNC CURR");
            return read_single();
        }

        //---------------------------------------------------------
        public void power(bool pwr)
        {
            string op = ":OUT" + channel + " ";

            if (pwr)
                op += "ON";
            else
                op += "OFF";

            write(op);
        }

        //---------------------------------------------------------
        //Control LCD display (off is faster)
        //A, B, DVMA, DVMB
        public void display(bool show)
        {
            string op = ":DISP:ENAB ";

            if (show)
                op += "ON";
            else
                op += "OFF";

            write(op);
        }

        //---------------------------------------------------------
        public void set_bandwidth(bandwidth bw)
        {
            op = ":OUTP" + channel + ":BAND ";
            op += Enum.GetName(typeof(bandwidth), bw);
            write(op);
        }

        //---------------------------------------------------------
        void set_current_res(current cur)
        {
            op = ":SENS" + channel + ":CURR:DC:RANG:UPP " + Enum.GetName(typeof(current), cur);
            write(op);
        }

        //---------------------------------------------------------
        // 0.00 to 1.00
        public void set_impedance(double impedance)
        {
            if ((impedance >= 0) && (impedance <= 1.0))
            {
                op = ":OUTP" + channel + ":IMP ";
                op += impedance.ToString("0.00");
                write(op);
            }
        }


        //---------------------------------------------------------
        public void set_voltage(double voltage)
        {
            op = ":SOUR" + channel + ":VOLT" + ":LEV:IMM:AMPL ";       //set immediately voltage level

            if ((voltage >= 0) & (voltage <= 15.0))
            {
                //format is 0.000 to 15.000
                op += voltage.ToString("E2");
                write(op);
            }
        }

        //---------------------------------------------------------
        // Set current limit
        //:SOUR:A:CURR:LIM:VAL 1.234
        public void set_current(double current)
        {
            op = ":SOUR" + channel + ":CURR" + ":LIM:VAL ";

            if ((current >= 0) & (current <= 5.0))        //2.5A max above 5V, can implement
            {
                //format is 0.000 to 5.000
                op += current.ToString("E2");
                write(op);
            }

        }

        //---------------------------------------------------------
        public void set_current_trip(current_trip trip)
        {
            string type = Enum.GetName(typeof(current_trip), trip);
            op = ":SOUR" + channel + ":CURR" + ":LIM:TYPE " + type;
            write(op);
        }

        //---------------------------------------------------------
        // Set interval between readings
        public void set_meas_interval(double interval)
        {
            op = ":SENS" + channel + ":MEAS:INT ";
            op += interval.ToString("E2");

            write(op);
        }

        //---------------------------------------------------------
        // Set number of averages for ??
        public void set_meas_averages(double averages)
        {
            op = ":SENS" + channel + ":AVER:COUN ";
            op += averages.ToString("E2");

            write(op);
        }

        //---------------------------------------------------------
        // Pulse length from 1-5000
        public void set_pulse_length(int readings)
        {
            op = ":SENS:PULS:SAMP:LENG ";
            op += readings.ToString("0");
            write(op);
        }

        //---------------------------------------------------------
        // Pulse sample interval from 0.01ms to 1s
        public void set_pulse_interval(double interval)
        {
            op = ":SENS:PULS:SAMP:INT ";
            op += interval.ToString("E2");
            write(op);
        }

        public void set_data_format(data_format fmt)
        {
            op = ":FORM:DATA " + Enum.GetName(typeof(data_format), fmt);
            write(op);
        }

        //---------------------------------------------------------
        // Trigger offset from -5000 to 50,000
        // Trigger count from 1 to 100
        //      - number of times the measurement is repeated to calculate average values (min, max, rms, etc.)
        // Trigger timeout from 0.001s to 60s, or INF

        //Sampling mode
        //0-60s, if using sampling mode, specifies time in seconds the sampling unit waits for trigger condition before
        //timeout message is generated after turning sampling mode ON.
        //
        public void set_trigger(int offset, int count, double timeout, trig_slope slope, trig_source source)
        {
            //offset
            op = ":SENS:PULS:TRIG:OFFS ";
            op += offset.ToString("0");
            write(op);

            //count
            op = ":SENS:PULS:TRIG:COUN ";
            op += count.ToString("0");
            write(op);

            //timeout
            op = ":SENS:PULS:TRIG:TIM ";
            if (timeout > 60)       //60s max so use greater as inf
                op += "INF";
            else
                op += timeout.ToString("E2");           //**FFF 0.00001 whatever seems to work so could use that if E2 doesnt work.
            write(op);

            //Slope
            op = ":SENS:PULS:TRIG:SLOP ";
            op += Enum.GetName(typeof(trig_slope), slope);
            write(op);

            //INT or EXTernal
            op = ":SENS:PULS:TRIG:SOUR ";
            op += Enum.GetName(typeof(trig_source), source);
            write(op);
        }


        //Sample start        [ON OFF]
        /*
        Setting the “Sample start” to ON initialises a sample or
analysis measurement.After “Sample start” has been set to ON the sampling circuit waits for a trigger event. This
can be either an internal or external trigger.After detecting the trigger condition, the input signal is sampled using the
given "Sample interval" (see A/B16) and the given "Sample length" (see A/B15).
The number of times this measurement cycle is repeated is given by the "Trigger count" (see A/B13). If "Trigger
timeout" (see (A/B10) is set to a value greater than 0 and no trigger condition is met after "Sampling start" is set to
ON for the time specified by "Trigger timeout" the error message "Trigger timeout" is displayed on the LCD.Both
conditions - a normal end of measurement or a "Trigger timeout" - will reset the "Sample start" to OFF.
*/
        private void pulse_state_on()
        {
            op = ":SENS:PULS:MEAS:STAR ON";
            write(op);
        }

        public void trigger()
        {
            //Sense pulse start on, then soft trigger to channel A
            op = "*ATRg";
            write(op);
        }

        //---------------------------------------------------------
        //read last triggered value
        public double read_single_last()
        {
            op = ":FETC" + channel + "?";
            write(op);
            return Convert.ToDouble(read());
        }

        //---------------------------------------------------------
        //read last triggered array values
        public double[] read_array_last()
        {
            string test;
            double[] x;

            //set_data_format(data_format.ASCII);
            //write(":FORM:BORD SWAP");

            op = ":FETC" + channel + ":ARR?";
            write(op);

            //maybe needs long timeout here?
            port.ReadTimeout = 9000;
            test = port.ReadLine();                 //not getting the right values here for some reason with ascii ( they were all the same. and with double unable to decode that into a useful number.. 

            //test.TrimStart('0');
            //test.TrimEnd('\n');
            x = Array.ConvertAll(test.Split(';'), double.Parse);
/*
            do
            {
                z = port.BytesToRead;
                System.Threading.Thread.Sleep(50);
            }
            while (z != port.BytesToRead);

            z -= 1;
            byte[] buf = new byte[z];
            double[] x = new double[z];
            port.Read(buf, 0, z);
            MemoryStream stream = new MemoryStream(buf);
            string tex = buf.ToString();
            // Create a reader using the stream from the writer.
            using (BinaryReader binReader =
                new BinaryReader(stream))

                try
                {
                    // Return to the beginning of the stream.
                    // Stream starts with '#0' 8 bytes number, next, etc.
                    binReader.BaseStream.Position = 2;

                    // Read
                    for (i = 0; i < z; i++)     //z / 8 bytes
                        x[i] = binReader.ReadDouble();
                }
                catch (EndOfStreamException e)
                {
                    Console.WriteLine("Error writing data: {0}.", e.GetType().Name);
                }*/


            port.ReadTimeout = 1000;

            return x;
        }

        //---------------------------------------------------------
        //trigger and then read
        public double read_single()
        {
            op = ":READ" + channel + "?";
            write(op);
            return Convert.ToDouble(read());
        }

        //need to make array of doubles
        public double[] read_array()
        {
            byte[] buf = { };
            double[] array = { };
            string input;

            op = ":READ" + channel + ":ARR?";
            write(op);


            input = port.ReadExisting();
            return array;
        }


        //---------------------------------------------------------
        //Can select Current, DVM, 
        //May be useful for hi-res sampling of some external voltage
        public void select_sample_channel(sample_ch ch)
        {
            string type = Enum.GetName(typeof(sample_ch), ch);
            op = ":SENS:PULS:MEAS:CHAN " + type;
            write(op);
        }


        //---------------------------------------------------------
        // Read the status byte register
        // MSB - > LSB
        // MEV, 1, EAV, QSB, MAV, ESB, RQS/MSS, OSB

        // MEV = reading available, overflow, etc.
        // EAV = Error available
        // QSB = cal something
        // MAV = message available?
        // ESB = event: query, command error, power on, etc.
        // RQS = any bit set in status byte
        // OSB = overvoltage, current limit trip, overheat, etc.
        public Int16 get_status()
        {
            write("*STB?");
            return Convert.ToInt16(read());
        }

        //---------------------------------------------------------
        //Read the event register and clear it
        public Int16 get_events()
        {
            write("*ESR?");
            return Convert.ToInt16(read());
        }

        //---------------------------------------------------------
        //Read the measurement condition register
        //ROFA
        private Int16 get_meas_reg()
        {
            write(":STAT:MEAS:COND?");
            return Convert.ToInt16(read());
        }


        public bool reading_available()
        {
            Int16 meas = get_meas_reg();
            int res = meas & (int)meas_COND.Reading_available_ch_a;

            //if(ch == channels.CH_A)
            return Convert.ToBoolean(res);

            if (res > 0)
                return true;
            else
                return false;
        }


        //---------------------------------------------------------
        //export array to csv
        //or to copy buffer or whatever
        void write(string str)
        {
            try
            {
                port.Write(str);        //Command string
                //port.Write("\r");       //CR terminator
                port.Write("\n");       //LF terminator
            }
            catch (Exception ex)
            {
                //close if error
            }
        }


        string read()
        {
            string buff;

            try
            {
                buff = port.ReadLine();
                return buff;
                //need to return something for error code, exception for convert to double if "" 
            }
            catch (Exception)
            {
                return "";
            }
        }

        public void read_bs()
        {
            try
            {
                string buff = port.ReadLine();
            }
            catch (Exception)
            {
            }
        }
    }
}
