﻿namespace ngmo_psu
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.myPort = new System.IO.Ports.SerialPort(this.components);
            this.lblVoltage = new System.Windows.Forms.Label();
            this.lblCurrent = new System.Windows.Forms.Label();
            this.gbxReadout = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblCurrentA = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblWatts = new System.Windows.Forms.Label();
            this.lblNGMOVersion = new System.Windows.Forms.Label();
            this.txtVoltage = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCurrent = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnApplyVI = new System.Windows.Forms.Button();
            this.rdioLimit = new System.Windows.Forms.RadioButton();
            this.rdioTrip = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.cboComPort = new System.Windows.Forms.ComboBox();
            this.tmrUpdateVI = new System.Windows.Forms.Timer(this.components);
            this.txtImpedance = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkOutput = new System.Windows.Forms.CheckBox();
            this.gbxSettings = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rdioBWHigh = new System.Windows.Forms.RadioButton();
            this.rdioBWLow = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gbxSettings2 = new System.Windows.Forms.GroupBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPulseInterval = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPulseLen = new System.Windows.Forms.TextBox();
            this.txtAverages = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnApply2 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMeasInterval = new System.Windows.Forms.TextBox();
            this.chkDisplay = new System.Windows.Forms.CheckBox();
            this.pltVoltage = new GraphLib.PlotterDisplayEx();
            this.gbxReadout.SuspendLayout();
            this.gbxSettings.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbxSettings2.SuspendLayout();
            this.SuspendLayout();
            // 
            // myPort
            // 
            this.myPort.BaudRate = 38400;
            this.myPort.DtrEnable = true;
            this.myPort.Handshake = System.IO.Ports.Handshake.RequestToSend;
            this.myPort.ReadBufferSize = 32768;
            this.myPort.RtsEnable = true;
            // 
            // lblVoltage
            // 
            this.lblVoltage.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVoltage.Location = new System.Drawing.Point(6, 15);
            this.lblVoltage.Name = "lblVoltage";
            this.lblVoltage.Size = new System.Drawing.Size(164, 45);
            this.lblVoltage.TabIndex = 0;
            this.lblVoltage.Text = "0.000";
            this.lblVoltage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCurrent
            // 
            this.lblCurrent.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrent.Location = new System.Drawing.Point(5, 56);
            this.lblCurrent.Name = "lblCurrent";
            this.lblCurrent.Size = new System.Drawing.Size(165, 45);
            this.lblCurrent.TabIndex = 1;
            this.lblCurrent.Text = "0.000";
            this.lblCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbxReadout
            // 
            this.gbxReadout.Controls.Add(this.label7);
            this.gbxReadout.Controls.Add(this.lblCurrentA);
            this.gbxReadout.Controls.Add(this.label5);
            this.gbxReadout.Controls.Add(this.lblWatts);
            this.gbxReadout.Controls.Add(this.lblVoltage);
            this.gbxReadout.Controls.Add(this.lblCurrent);
            this.gbxReadout.Location = new System.Drawing.Point(12, 6);
            this.gbxReadout.Name = "gbxReadout";
            this.gbxReadout.Size = new System.Drawing.Size(259, 152);
            this.gbxReadout.TabIndex = 3;
            this.gbxReadout.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(190, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 45);
            this.label7.TabIndex = 6;
            this.label7.Text = "V";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCurrentA
            // 
            this.lblCurrentA.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrentA.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentA.Location = new System.Drawing.Point(170, 56);
            this.lblCurrentA.Name = "lblCurrentA";
            this.lblCurrentA.Size = new System.Drawing.Size(88, 45);
            this.lblCurrentA.TabIndex = 5;
            this.lblCurrentA.Text = "mA";
            this.lblCurrentA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(183, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 45);
            this.label5.TabIndex = 4;
            this.label5.Text = "W";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblWatts
            // 
            this.lblWatts.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWatts.Location = new System.Drawing.Point(6, 98);
            this.lblWatts.Name = "lblWatts";
            this.lblWatts.Size = new System.Drawing.Size(164, 45);
            this.lblWatts.TabIndex = 3;
            this.lblWatts.Text = "0.000";
            this.lblWatts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNGMOVersion
            // 
            this.lblNGMOVersion.Enabled = false;
            this.lblNGMOVersion.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblNGMOVersion.Location = new System.Drawing.Point(465, 267);
            this.lblNGMOVersion.Name = "lblNGMOVersion";
            this.lblNGMOVersion.Size = new System.Drawing.Size(244, 13);
            this.lblNGMOVersion.TabIndex = 2;
            this.lblNGMOVersion.Text = "NGMO Version: ";
            this.lblNGMOVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtVoltage
            // 
            this.txtVoltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVoltage.Location = new System.Drawing.Point(10, 19);
            this.txtVoltage.MaxLength = 6;
            this.txtVoltage.Name = "txtVoltage";
            this.txtVoltage.Size = new System.Drawing.Size(45, 22);
            this.txtVoltage.TabIndex = 4;
            this.txtVoltage.Text = "3.300";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(61, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "V";
            // 
            // txtCurrent
            // 
            this.txtCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrent.Location = new System.Drawing.Point(10, 45);
            this.txtCurrent.MaxLength = 5;
            this.txtCurrent.Name = "txtCurrent";
            this.txtCurrent.Size = new System.Drawing.Size(45, 22);
            this.txtCurrent.TabIndex = 6;
            this.txtCurrent.Text = "0.200";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(61, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "A";
            // 
            // btnApplyVI
            // 
            this.btnApplyVI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApplyVI.Location = new System.Drawing.Point(91, 19);
            this.btnApplyVI.Name = "btnApplyVI";
            this.btnApplyVI.Size = new System.Drawing.Size(77, 48);
            this.btnApplyVI.TabIndex = 8;
            this.btnApplyVI.Text = "Apply";
            this.btnApplyVI.UseVisualStyleBackColor = true;
            this.btnApplyVI.Click += new System.EventHandler(this.btnApplyVI_Click);
            // 
            // rdioLimit
            // 
            this.rdioLimit.AutoSize = true;
            this.rdioLimit.Checked = true;
            this.rdioLimit.Location = new System.Drawing.Point(17, 18);
            this.rdioLimit.Name = "rdioLimit";
            this.rdioLimit.Size = new System.Drawing.Size(46, 17);
            this.rdioLimit.TabIndex = 10;
            this.rdioLimit.TabStop = true;
            this.rdioLimit.Text = "Limit";
            this.rdioLimit.UseVisualStyleBackColor = true;
            this.rdioLimit.CheckedChanged += new System.EventHandler(this.rdioLimit_CheckedChanged);
            // 
            // rdioTrip
            // 
            this.rdioTrip.AutoSize = true;
            this.rdioTrip.Location = new System.Drawing.Point(17, 41);
            this.rdioTrip.Name = "rdioTrip";
            this.rdioTrip.Size = new System.Drawing.Size(43, 17);
            this.rdioTrip.TabIndex = 11;
            this.rdioTrip.Text = "Trip";
            this.rdioTrip.UseVisualStyleBackColor = true;
            this.rdioTrip.CheckedChanged += new System.EventHandler(this.rdioTrip_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Current limiting:";
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Location = new System.Drawing.Point(629, 283);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(80, 23);
            this.btnConnect.TabIndex = 13;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // cboComPort
            // 
            this.cboComPort.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboComPort.FormattingEnabled = true;
            this.cboComPort.Location = new System.Drawing.Point(547, 283);
            this.cboComPort.Name = "cboComPort";
            this.cboComPort.Size = new System.Drawing.Size(76, 23);
            this.cboComPort.TabIndex = 14;
            this.cboComPort.Text = "COMx";
            this.cboComPort.Click += new System.EventHandler(this.cboComPort_Click);
            // 
            // tmrUpdateVI
            // 
            this.tmrUpdateVI.Interval = 500;
            this.tmrUpdateVI.Tick += new System.EventHandler(this.tmrUpdateVI_Tick);
            // 
            // txtImpedance
            // 
            this.txtImpedance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImpedance.Location = new System.Drawing.Point(10, 73);
            this.txtImpedance.MaxLength = 4;
            this.txtImpedance.Name = "txtImpedance";
            this.txtImpedance.Size = new System.Drawing.Size(46, 22);
            this.txtImpedance.TabIndex = 15;
            this.txtImpedance.Text = "0.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(61, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 16);
            this.label4.TabIndex = 16;
            this.label4.Text = "Ω";
            // 
            // chkOutput
            // 
            this.chkOutput.BackColor = System.Drawing.Color.Transparent;
            this.chkOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOutput.Location = new System.Drawing.Point(91, 74);
            this.chkOutput.Name = "chkOutput";
            this.chkOutput.Size = new System.Drawing.Size(86, 22);
            this.chkOutput.TabIndex = 17;
            this.chkOutput.Text = "Output";
            this.chkOutput.UseVisualStyleBackColor = false;
            this.chkOutput.CheckedChanged += new System.EventHandler(this.chkOutput_CheckedChanged);
            // 
            // gbxSettings
            // 
            this.gbxSettings.Controls.Add(this.txtVoltage);
            this.gbxSettings.Controls.Add(this.chkOutput);
            this.gbxSettings.Controls.Add(this.label1);
            this.gbxSettings.Controls.Add(this.label4);
            this.gbxSettings.Controls.Add(this.txtCurrent);
            this.gbxSettings.Controls.Add(this.txtImpedance);
            this.gbxSettings.Controls.Add(this.label2);
            this.gbxSettings.Controls.Add(this.btnApplyVI);
            this.gbxSettings.Enabled = false;
            this.gbxSettings.Location = new System.Drawing.Point(277, 6);
            this.gbxSettings.Name = "gbxSettings";
            this.gbxSettings.Size = new System.Drawing.Size(179, 152);
            this.gbxSettings.TabIndex = 18;
            this.gbxSettings.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Bandwidth:";
            // 
            // rdioBWHigh
            // 
            this.rdioBWHigh.AutoSize = true;
            this.rdioBWHigh.Checked = true;
            this.rdioBWHigh.Location = new System.Drawing.Point(17, 17);
            this.rdioBWHigh.Name = "rdioBWHigh";
            this.rdioBWHigh.Size = new System.Drawing.Size(47, 17);
            this.rdioBWHigh.TabIndex = 20;
            this.rdioBWHigh.TabStop = true;
            this.rdioBWHigh.Text = "High";
            this.rdioBWHigh.UseVisualStyleBackColor = true;
            this.rdioBWHigh.CheckedChanged += new System.EventHandler(this.rdioBWHigh_CheckedChanged);
            // 
            // rdioBWLow
            // 
            this.rdioBWLow.AutoSize = true;
            this.rdioBWLow.Location = new System.Drawing.Point(17, 40);
            this.rdioBWLow.Name = "rdioBWLow";
            this.rdioBWLow.Size = new System.Drawing.Size(45, 17);
            this.rdioBWLow.TabIndex = 21;
            this.rdioBWLow.Text = "Low";
            this.rdioBWLow.UseVisualStyleBackColor = true;
            this.rdioBWLow.CheckedChanged += new System.EventHandler(this.rdioBWLow_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rdioBWHigh);
            this.panel1.Controls.Add(this.rdioBWLow);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Location = new System.Drawing.Point(6, 87);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(76, 63);
            this.panel1.TabIndex = 22;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rdioLimit);
            this.panel2.Controls.Add(this.rdioTrip);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(6, 18);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(76, 63);
            this.panel2.TabIndex = 23;
            // 
            // gbxSettings2
            // 
            this.gbxSettings2.Controls.Add(this.lblStatus);
            this.gbxSettings2.Controls.Add(this.btnStart);
            this.gbxSettings2.Controls.Add(this.label13);
            this.gbxSettings2.Controls.Add(this.txtPulseInterval);
            this.gbxSettings2.Controls.Add(this.label12);
            this.gbxSettings2.Controls.Add(this.label11);
            this.gbxSettings2.Controls.Add(this.txtPulseLen);
            this.gbxSettings2.Controls.Add(this.txtAverages);
            this.gbxSettings2.Controls.Add(this.label10);
            this.gbxSettings2.Controls.Add(this.label9);
            this.gbxSettings2.Controls.Add(this.btnApply2);
            this.gbxSettings2.Controls.Add(this.label8);
            this.gbxSettings2.Controls.Add(this.txtMeasInterval);
            this.gbxSettings2.Controls.Add(this.chkDisplay);
            this.gbxSettings2.Controls.Add(this.panel2);
            this.gbxSettings2.Controls.Add(this.panel1);
            this.gbxSettings2.Enabled = false;
            this.gbxSettings2.Location = new System.Drawing.Point(462, 6);
            this.gbxSettings2.Name = "gbxSettings2";
            this.gbxSettings2.Size = new System.Drawing.Size(247, 250);
            this.gbxSettings2.TabIndex = 24;
            this.gbxSettings2.TabStop = false;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(9, 187);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(43, 13);
            this.lblStatus.TabIndex = 35;
            this.lblStatus.Text = "Status: ";
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(161, 213);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(77, 31);
            this.btnStart.TabIndex = 34;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(218, 99);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "ms";
            // 
            // txtPulseInterval
            // 
            this.txtPulseInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPulseInterval.Location = new System.Drawing.Point(179, 96);
            this.txtPulseInterval.MaxLength = 4;
            this.txtPulseInterval.Name = "txtPulseInterval";
            this.txtPulseInterval.Size = new System.Drawing.Size(33, 20);
            this.txtPulseInterval.TabIndex = 32;
            this.txtPulseInterval.Text = "0.01";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(100, 99);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Pulse interval:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(100, 73);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "Pulse length:";
            // 
            // txtPulseLen
            // 
            this.txtPulseLen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPulseLen.Location = new System.Drawing.Point(179, 70);
            this.txtPulseLen.MaxLength = 4;
            this.txtPulseLen.Name = "txtPulseLen";
            this.txtPulseLen.Size = new System.Drawing.Size(33, 20);
            this.txtPulseLen.TabIndex = 29;
            this.txtPulseLen.Text = "20";
            // 
            // txtAverages
            // 
            this.txtAverages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAverages.Location = new System.Drawing.Point(179, 44);
            this.txtAverages.MaxLength = 2;
            this.txtAverages.Name = "txtAverages";
            this.txtAverages.Size = new System.Drawing.Size(33, 20);
            this.txtAverages.TabIndex = 28;
            this.txtAverages.Text = "1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(100, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "Averages:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(218, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(20, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "ms";
            // 
            // btnApply2
            // 
            this.btnApply2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply2.Location = new System.Drawing.Point(161, 179);
            this.btnApply2.Name = "btnApply2";
            this.btnApply2.Size = new System.Drawing.Size(77, 28);
            this.btnApply2.TabIndex = 18;
            this.btnApply2.Text = "Apply";
            this.btnApply2.UseVisualStyleBackColor = true;
            this.btnApply2.Click += new System.EventHandler(this.btnApply2_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(100, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Meas interval:";
            // 
            // txtMeasInterval
            // 
            this.txtMeasInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMeasInterval.Location = new System.Drawing.Point(179, 18);
            this.txtMeasInterval.MaxLength = 3;
            this.txtMeasInterval.Name = "txtMeasInterval";
            this.txtMeasInterval.Size = new System.Drawing.Size(33, 20);
            this.txtMeasInterval.TabIndex = 25;
            this.txtMeasInterval.Text = "10";
            // 
            // chkDisplay
            // 
            this.chkDisplay.AutoSize = true;
            this.chkDisplay.Location = new System.Drawing.Point(23, 156);
            this.chkDisplay.Name = "chkDisplay";
            this.chkDisplay.Size = new System.Drawing.Size(60, 17);
            this.chkDisplay.TabIndex = 24;
            this.chkDisplay.Text = "Display";
            this.chkDisplay.UseVisualStyleBackColor = true;
            this.chkDisplay.CheckedChanged += new System.EventHandler(this.chkDisplay_CheckedChanged);
            // 
            // pltVoltage
            // 
            this.pltVoltage.BackColor = System.Drawing.Color.Transparent;
            this.pltVoltage.BackgroundColorBot = System.Drawing.Color.White;
            this.pltVoltage.BackgroundColorTop = System.Drawing.Color.White;
            this.pltVoltage.DashedGridColor = System.Drawing.Color.DarkGray;
            this.pltVoltage.DoubleBuffering = false;
            this.pltVoltage.Location = new System.Drawing.Point(12, 123);
            this.pltVoltage.Name = "pltVoltage";
            this.pltVoltage.PlaySpeed = 0.5F;
            this.pltVoltage.Size = new System.Drawing.Size(442, 176);
            this.pltVoltage.SolidGridColor = System.Drawing.Color.DarkGray;
            this.pltVoltage.TabIndex = 25;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(717, 311);
            this.Controls.Add(this.gbxReadout);
            this.Controls.Add(this.gbxSettings);
            this.Controls.Add(this.pltVoltage);
            this.Controls.Add(this.gbxSettings2);
            this.Controls.Add(this.cboComPort);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.lblNGMOVersion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "NGMO supply";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.gbxReadout.ResumeLayout(false);
            this.gbxReadout.PerformLayout();
            this.gbxSettings.ResumeLayout(false);
            this.gbxSettings.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gbxSettings2.ResumeLayout(false);
            this.gbxSettings2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.IO.Ports.SerialPort myPort;
        private System.Windows.Forms.Label lblVoltage;
        private System.Windows.Forms.Label lblCurrent;
        private System.Windows.Forms.GroupBox gbxReadout;
        private System.Windows.Forms.TextBox txtVoltage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCurrent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnApplyVI;
        private System.Windows.Forms.RadioButton rdioLimit;
        private System.Windows.Forms.RadioButton rdioTrip;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.ComboBox cboComPort;
        private System.Windows.Forms.Timer tmrUpdateVI;
        private System.Windows.Forms.TextBox txtImpedance;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblNGMOVersion;
        private System.Windows.Forms.Label lblWatts;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblCurrentA;
        private System.Windows.Forms.CheckBox chkOutput;
        private System.Windows.Forms.GroupBox gbxSettings;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton rdioBWHigh;
        private System.Windows.Forms.RadioButton rdioBWLow;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox gbxSettings2;
        private System.Windows.Forms.CheckBox chkDisplay;
        private System.Windows.Forms.Button btnApply2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtMeasInterval;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAverages;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPulseLen;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPulseInterval;
        private System.Windows.Forms.Label label12;
        private GraphLib.PlotterDisplayEx pltVoltage;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label lblStatus;
    }
}

