﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace ngmo_psu
{
    using GraphLib;

    public partial class frmMain : Form
    {
        ngmo psu;
        private String CurColorSchema = "GRAY";

        public frmMain()
        {
            InitializeComponent();
            cboComPort.DropDownStyle = ComboBoxStyle.DropDownList;
            psu = new ngmo();

            pltVoltage.Smoothing = System.Drawing.Drawing2D.SmoothingMode.None;
            CalcDataGraphs();
            pltVoltage.Refresh();

            tmrUpdateVI.Interval = 250;
        }

        private void btnApplyVI_Click(object sender, EventArgs e)
        {
            double voltage = 0, current = 0, impedance = 0;
            voltage = ToDouble(txtVoltage.Text);
            current = ToDouble(txtCurrent.Text);
            impedance = ToDouble(txtImpedance.Text);

            if (voltage > 15.0)
                voltage = 15;
            else if (voltage < 0)
                voltage = 0;

            if (current > 5.0)
                current = 5;
            else if (current < 0)
                current = 0;

            if (impedance > 1.0)
                impedance = 1;
            else if (impedance < 0)
                impedance = 0;

            //Send current setting
            psu.set_voltage(voltage);
            psu.set_current(current);
            psu.set_impedance(impedance);

            txtVoltage.Text = voltage.ToString("0.000");
            txtCurrent.Text = current.ToString("0.000");
            txtImpedance.Text = impedance.ToString("0.00");
        }

        private void cboComPort_Click(object sender, EventArgs e)
        {
            string[] port_names;

            cboComPort.Items.Clear();
            port_names = SerialPort.GetPortNames();

            foreach (var item in port_names)
            {
                cboComPort.Items.Add(item);
            }

            //Select highest port **FFF
            //cboComPort.SelectedText = "COM4";
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (btnConnect.Text == "Connect")
            {
                try
                {
                    myPort.PortName = cboComPort.SelectedItem.ToString();
                    myPort.Open();
                    lblNGMOVersion.Text = psu.initialize(myPort);

                    if (psu.connected)
                    {
                        psu.clear_all();
                        btnApplyVI_Click(sender, e);    //Make sure voltages shown are up to date
                        psu.display(false);             //disable display
                    }
                    else
                    {
                        lblNGMOVersion.Text = "Connection failed";
                        myPort.Close();
                        cboComPort.Enabled = true;
                        return;
                    }
                }
                catch (Exception ex)
                {
                    string test = ex.ToString();
                    return;
                }

                //tmrUpdateVI.Enabled = true;
                cboComPort.Enabled = false;
                btnConnect.Text = "Disconnect";
                gbxSettings.Enabled = true;
                gbxSettings2.Enabled = true;
            }

            else
            {
                myPort.Close();
                cboComPort.Enabled = true;
                btnConnect.Text = "Connect";
                lblNGMOVersion.Text = "";
                tmrUpdateVI.Enabled = false;
                gbxSettings.Enabled = false;
                gbxSettings2.Enabled = false;
            }

        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (myPort.IsOpen)
            {
                myPort.Close();
            }
        }

        private void tmrUpdateVI_Tick(object sender, EventArgs e)
        {
            if (btnConnect.Text == "Disconnect")
            {
                double voltage, current;
                Int16 status;

                status = psu.get_status();
                voltage = psu.get_voltage();
                current = psu.get_current();
                lblVoltage.Text = voltage.ToString("0.000");
                lblWatts.Text = (voltage * current).ToString("0.000");

                if ((current < 0.01) & (current > -0.01))
                {
                    current *= 1000;
                    lblCurrentA.Text = "mA";
                    lblCurrent.Text = current.ToString("0.000");
                }
                else if ((current < 1.000) & (current > -1.000))
                {
                    current *= 1000;
                    lblCurrentA.Text = "mA";
                    lblCurrent.Text = current.ToString("0.0");
                }
                else  //>1.000
                {
                    lblCurrentA.Text = "A";
                    lblCurrent.Text = current.ToString("0.000");
                }


                lblStatus.Text = "Status: " + status.ToString("X");
            }
        }

        private void chkOutput_CheckedChanged(object sender, EventArgs e)
        {
            //maybe apply settings first 
            btnApplyVI_Click(sender, e);

            if (chkOutput.Checked)
                psu.power(true);
            else
                psu.power(false);
        }

        private void rdioTrip_CheckedChanged(object sender, EventArgs e)
        {
            if (rdioTrip.Checked)
                psu.set_current_trip(ngmo.current_trip.TRIP);
        }

        private void rdioLimit_CheckedChanged(object sender, EventArgs e)
        {
            if (rdioLimit.Checked)
                psu.set_current_trip(ngmo.current_trip.LIMIT);
        }

        private void rdioBWHigh_CheckedChanged(object sender, EventArgs e)
        {
            if (rdioBWHigh.Checked)
                psu.set_bandwidth(ngmo.bandwidth.HIGH);
        }

        private void rdioBWLow_CheckedChanged(object sender, EventArgs e)
        {
            if (rdioBWLow.Checked)
                psu.set_bandwidth(ngmo.bandwidth.LOW);
        }

        private void chkDisplay_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDisplay.Checked)
                psu.display(true);
            else
                psu.display(false);
        }

        private void btnApply2_Click(object sender, EventArgs e)
        {
            double interval, averages, length, pulseInterval;

            interval = ToDouble(txtMeasInterval.Text);
            averages = ToDouble(txtAverages.Text);
            length = ToDouble(txtPulseLen.Text);
            pulseInterval = ToDouble(txtPulseInterval.Text);

            if (interval < 2)
                interval = 2;
            else if (interval > 200)
                interval = 200;

            psu.set_meas_interval(interval/1000);            //Send interval in ms
            txtMeasInterval.Text = interval.ToString("0");

            if (averages < 1)
                averages = 1;
            else if (averages > 10)
                averages = 10;

            psu.set_meas_averages(averages);
            txtAverages.Text = averages.ToString("0");

            if (length < 1)
                length = 1;
            if (length > 5000)
                length = 5000;

            psu.set_pulse_length(Convert.ToInt32(length));
            txtPulseLen.Text = length.ToString("0");


            if (pulseInterval < 0.01)
                pulseInterval = 0.01;
            if (pulseInterval > 1000)
                pulseInterval = 1000;

            psu.set_pulse_interval(pulseInterval / 1000);
            txtPulseInterval.Text = pulseInterval.ToString("0.00");
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            tmrUpdateVI.Enabled = false;        //disable screen update while reading array
            psu.read_bs();
            
            //Ensure capture settings up to date
            //btnApply2_Click(sender, e);
            //psu.set_data_format(ngmo.data_format.ASCII);        //try to get it working with ascii first, then change it later..
            //psu.select_sample_channel(ngmo.sample_ch.CURR);
            psu.trigger(); //ATRG

            //Wait for measurement ready flag, then read array
            int i = 0;
            while(psu.reading_available() == false)
            {
                System.Threading.Thread.Sleep(100);
                //update form?
                if (i++ > 50)
                    return;
            }
                 psu.read_array_last();

            tmrUpdateVI.Enabled = true;
        }

        double ToDouble(string input)
        {
            try
            {
                return Convert.ToDouble(input);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        protected void CalcDataGraphs()
        {
            int j = 0;
            this.SuspendLayout();

            pltVoltage.DataSources.Clear();
            pltVoltage.SetDisplayRangeX(0, 400);

            pltVoltage.DataSources.Add(new DataSource());
            pltVoltage.DataSources[j].Name = "Current (mA)";
            pltVoltage.DataSources[j].OnRenderXAxisLabel += RenderXLabel;


            //case "NORMAL_AUTO":
            //this.Text = "Normal Graph Autoscaled";
            pltVoltage.DataSources[j].Length = 1000;            //Set to number of samples
            pltVoltage.PanelLayout = PlotterGraphPaneEx.LayoutMode.NORMAL;
            pltVoltage.DataSources[j].AutoScaleY = true;
            pltVoltage.DataSources[j].AutoScaleX = true;
            pltVoltage.DataSources[j].SetDisplayRangeY(0, 10);
            pltVoltage.DataSources[j].SetGridDistanceY(10);
            pltVoltage.DataSources[j].OnRenderYAxisLabel = RenderYLabel;
            
            //CalcSinusFunction_0(pltVoltage.DataSources[j], j);
            save_datapoints(pltVoltage.DataSources[0]);


            ApplyColorSchema();

            this.ResumeLayout();
            pltVoltage.Refresh();
        }


        private void ApplyColorSchema()
        {
            switch (CurColorSchema)
            {
                case "DARK_GREEN":
                    {
                        Color[] cols = { Color.FromArgb(0,255,0),
                                         Color.FromArgb(0,255,0),
                                         Color.FromArgb(0,255,0),
                                         Color.FromArgb(0,255,0),
                                         Color.FromArgb(0,255,0) ,
                                         Color.FromArgb(0,255,0),
                                         Color.FromArgb(0,255,0) };


                        pltVoltage.DataSources[0].GraphColor = cols[0 % 7];
                        pltVoltage.BackgroundColorTop = Color.FromArgb(0, 64, 0);
                        pltVoltage.BackgroundColorBot = Color.FromArgb(0, 64, 0);
                        pltVoltage.SolidGridColor = Color.FromArgb(0, 128, 0);
                        pltVoltage.DashedGridColor = Color.FromArgb(0, 128, 0);
                    }
                    break;
                case "WHITE":
                    {
                        Color[] cols = { Color.DarkRed,
                                         Color.DarkSlateGray,
                                         Color.DarkCyan,
                                         Color.DarkGreen,
                                         Color.DarkBlue ,
                                         Color.DarkMagenta,
                                         Color.DeepPink };


                        pltVoltage.DataSources[0].GraphColor = cols[0 % 7];

                        pltVoltage.BackgroundColorTop = Color.White;
                        pltVoltage.BackgroundColorBot = Color.White;
                        pltVoltage.SolidGridColor = Color.LightGray;
                        pltVoltage.DashedGridColor = Color.LightGray;
                    }
                    break;

                case "BLUE":
                    {
                        Color[] cols = { Color.Red,
                                         Color.Orange,
                                         Color.Yellow,
                                         Color.LightGreen,
                                         Color.Blue ,
                                         Color.DarkSalmon,
                                         Color.LightPink };


                        pltVoltage.DataSources[0].GraphColor = cols[0 % 7];

                        pltVoltage.BackgroundColorTop = Color.Navy;
                        pltVoltage.BackgroundColorBot = Color.FromArgb(0, 0, 64);
                        pltVoltage.SolidGridColor = Color.Blue;
                        pltVoltage.DashedGridColor = Color.Blue;
                    }
                    break;

                case "GRAY":
                    {
                        Color[] cols = { Color.DarkRed,
                                         Color.DarkSlateGray,
                                         Color.DarkCyan,
                                         Color.DarkGreen,
                                         Color.DarkBlue ,
                                         Color.DarkMagenta,
                                         Color.DeepPink };


                        pltVoltage.DataSources[0].GraphColor = cols[0 % 7];
                        pltVoltage.BackgroundColorTop = Color.White;
                        pltVoltage.BackgroundColorBot = Color.LightGray;
                        pltVoltage.SolidGridColor = Color.LightGray;
                        pltVoltage.DashedGridColor = Color.LightGray;
                    }
                    break;

                case "RED":
                    {
                        Color[] cols = { Color.DarkCyan,
                                         Color.Yellow,
                                         Color.DarkCyan,
                                         Color.DarkGreen,
                                         Color.DarkBlue ,
                                         Color.DarkMagenta,
                                         Color.DeepPink };

                        pltVoltage.DataSources[0].GraphColor = cols[0 % 7];


                        pltVoltage.BackgroundColorTop = Color.DarkRed;
                        pltVoltage.BackgroundColorBot = Color.Black;
                        pltVoltage.SolidGridColor = Color.Red;
                        pltVoltage.DashedGridColor = Color.Red;
                    }
                    break;

                case "LIGHT_BLUE":
                    {
                        Color[] cols = { Color.DarkRed,
                                         Color.DarkSlateGray,
                                         Color.DarkCyan,
                                         Color.DarkGreen,
                                         Color.DarkBlue ,
                                         Color.DarkMagenta,
                                         Color.DeepPink };

                        pltVoltage.DataSources[0].GraphColor = cols[0 % 7];
                        pltVoltage.BackgroundColorTop = Color.White;
                        pltVoltage.BackgroundColorBot = Color.FromArgb(183, 183, 255);
                        pltVoltage.SolidGridColor = Color.Blue;
                        pltVoltage.DashedGridColor = Color.Blue;
                    }
                    break;

                case "BLACK":
                    {
                        Color[] cols = { Color.FromArgb(255,0,0),
                                         Color.FromArgb(0,255,0),
                                         Color.FromArgb(255,255,0),
                                         Color.FromArgb(64,64,255),
                                         Color.FromArgb(0,255,255) ,
                                         Color.FromArgb(255,0,255),
                                         Color.FromArgb(255,128,0) };

                        pltVoltage.DataSources[0].GraphColor = cols[0 % 7];
                        pltVoltage.BackgroundColorTop = Color.Black;
                        pltVoltage.BackgroundColorBot = Color.Black;
                        pltVoltage.SolidGridColor = Color.DarkGray;
                        pltVoltage.DashedGridColor = Color.DarkGray;
                    }
                    break;
            }

        }

        private String RenderXLabel(DataSource s, int idx)
        {
            if (s.AutoScaleX)
            {
                //if (idx % 2 == 0)
                {
                    int Value = (int)(s.Samples[idx].x);
                    return "" + Value;
                }
                return "";
            }
            else
            {
                int Value = (int)(s.Samples[idx].x / 200);
                String Label = "" + Value + "\"";
                return Label;
            }
        }

        private String RenderYLabel(DataSource s, float value)
        {
            return String.Format("{0:0.0}", value);
        }


        protected void save_datapoints(DataSource src)
        {
            for (int i = 0; i < src.Length; i++)
            {
                src.Samples[i].x = i;       //Can calculate x depending on time settings ms
                src.Samples[i].y = i;
            }
        }


    }
}
